﻿IF NOT EXISTS(SELECT * FROM [dbo].[Product])
BEGIN  
    SET IDENTITY_INSERT [dbo].[Product] ON;
    INSERT INTO [dbo].[Product] ([Id],[Name],[Description],[Weight],[Height],[Width],[Length]) VALUES (1,'Phone','Mobile phone',200,150,5.05,200);
    INSERT INTO [dbo].[Product] ([Id],[Name],[Description],[Weight],[Height],[Width],[Length]) VALUES (2,'PC','Computer',800,1060,560,500.05);
    INSERT INTO [dbo].[Product] ([Id],[Name],[Description],[Weight],[Height],[Width],[Length]) VALUES (3,'Laptop','Laptop',230,670,20.3,266.63);
    INSERT INTO [dbo].[Product] ([Id],[Name],[Description],[Weight],[Height],[Width],[Length]) VALUES (4,'Clock','Clock',50,55.7,65,20.78);
    SET IDENTITY_INSERT [dbo].[Product] OFF;
END 

IF NOT EXISTS(SELECT * FROM [dbo].[Order])
BEGIN  
    SET IDENTITY_INSERT [dbo].[Order] ON;
    INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES (1,6,'2022-05-25','2022-05-28',2);
    INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES (2,2,'2022-06-17','2022-05-20',3);
    INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES (3,4,'2022-05-12','2022-05-25',3);
    INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES (4,3,'2022-04-14','2022-05-28',4);
    INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES (5,5,'2022-06-02','2022-06-15',1);
    INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES (6,2,'2022-05-25','2022-05-28',2);
    SET IDENTITY_INSERT [dbo].[Order] OFF;
END