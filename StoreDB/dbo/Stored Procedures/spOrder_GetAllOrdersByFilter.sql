﻿CREATE PROCEDURE [dbo].[spOrder_GetAllOrdersByFilter]
	@Month INT = NULL,
	@Status INT = NULL,
	@Year INT = NULL,
	@ProductId INT = NULL
AS
BEGIN
	SELECT * FROM [dbo].[Order] 
	WHERE 
		((YEAR([dbo].[Order].[CreatedDate]) = @Year OR YEAR([Order].[UpdatedDate]) = @Year) OR @Year IS NULL)
		AND ((MONTH([dbo].[Order].[CreatedDate]) = @Month OR MONTH([dbo].[Order].[UpdatedDate]) = @Month) OR @Month IS NULL)
		AND ([dbo].[Order].[Status] = @Status OR @Status IS NULL)
		AND ([dbo].[Order].[ProductId] = @ProductId OR @ProductId IS NULL)
END
