﻿CREATE TABLE [dbo].[Order]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Status] INT NOT NULL,
    [CreatedDate] DATE NOT NULL,
    [UpdatedDate] DATE NOT NULL,
    [ProductId] INT NOT NULL,
    CONSTRAINT [FK_Order_Product] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id])
)
